# Max Tile Project


## Presentation

[Introduction to the project]

This project repository contains three distinct components:

 - an HLS project, at directory `hls`
 - a Vivado project, at directory `vivado`
 - a Petalinux project, at directory `petalinux`

## Prerequisites

You need to have:

 - Vivado + Vivado HLS (tested 2017.4)
 - Petalinux tools (tested 2017.4)

You need to have sourced the configuration files for both Vivado/HLS and Petalinux.
Usually, Vivado is sourced with:

```
source /opt/Xilinx/2017.4/Vivado/settings64.sh
```

and Petalinux tools are sourced with:

```
source /opt/pkg/petalinux/settings.sh
```

## Building

First, build the HLS IP, in directory `hls`:

(for the moment, no makefile for HLS)

Then, build the Vivado project, in directory `vivado`:

```
make
```

should do it. In order to just create the project to later modify it with vivado, run:

```
vivado -mode tcl -source create_project.tcl
```

Then, build Petalinux in directory `petalinux`:

```
make
```

## Executing

### Booting the Zynq off an sdcard

If you want to use an sdcard, run in directory `petalinux`:

```
make sdcard
```

Have an sdcard ready, with the first partition formatted as fat32. Then, cd into the `images/linux` directory, and copy the two files `BOOT.bin` and `image.ub` to the sdcard.

Set the boot jumper to sdcard, insert the sdcardm, plug the Zynq and you're ready to go.
The zynq's console is accessed through a 115200-baud serial port which is the second one provided by the USB interface. You can run this as root:

```
screen /dev/ttyUSB1 115200
```

### Booting off JTAG

Set the Zynq boot jumper to JTAG. Connect the Zynq to a computer on which you are root. You need to have the Xilinx toolchain installed on that computer and sourced.
Run the hardware server as root:

```
hw_server
```

A hardware server will be listening on port 3121 of that machine. Additionnally, look for the Zynq's console (assuming the UART is available at `ttyUSB1`):

```
screen /dev/ttyUSB1 115200
```

Then, run the command that follows from the `petalinux` directory:

```
petalinux-boot --jtag --hw_server-url TCP:IP:port --fpga --bitstream images/linux/master_wrapper.bit --kernel -i images/linux/image.ub
```

where `IP` is the IP address of the computer the Zynq is plugged in to, and `port` is usually 3121. 

If you are compiling from a department/university machine on which you are not root, your own laptop may do the stuff. Since your laptop may not be reachable from the shared machine (WiFi in universities is often usign NATs and is firewalled, preventing you from reaching port 3121 even if no NAT is used), I suggest you use a reverse SSH tunnel towards the department machine, as follows:

On your laptop, run:
```
ssh -N -R 45590:localhost:3121 user@department_machine
```

and then to boot the board, from the department machine:
```
petalinux-boot --jtag --hw_server-url TCP:localhost:45590 --fpga --bitstream images/linux/master_wrapper.bit --kernel -i images/linux/image.ub
```

### Executing the program

On the Zynq's console, login as root (password is root). Then, run
```
maxtile
```
and watch the magic happen.
