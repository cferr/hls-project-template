#ifndef _HLS_UTILS_H
#define _HLS_UTILS_H

#include <stdlib.h>
#include <stdint.h>

void* malloc_fpga(size_t size, uint32_t* offset_ptr);
void* calloc_fpga(size_t nmemb, size_t size, uint32_t* offset_ptr);

#endif
