#!/bin/bash

cat >hls.bb << EOF

SUMMARY = "Simple hls application"
SECTION = "PETALINUX/apps"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://\${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

EOF
echo -ne 'SRC_URI = "' >> hls.bb

for i in ./*; do
    echo "file://"$(basename $i)" \\">> hls.bb
done

cat >>hls.bb << EOF
"

S = "\${WORKDIR}"

do_compile() {
	     oe_runmake
}

do_install() {
	     install -d \${D}\${bindir}
	     install -m 0755 hls \${D}\${bindir}
	     install -d \${D}/root
	     install -m 0644 in.bmp \${D}/root
}
EOF
