#include "utils.h"
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <stdint.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>

// Most basic allocator : allocate once, increment alloc counter
// TODO enhance this...

#define HLS_WRAPPER_RESERVED_SPACE_BASE 0x20000000
#define HLS_WRAPPER_RESERVED_SPACE_HIGH 0x23FFFFFF
#define HLS_WRAPPER_RESERVED_SPACE_SIZE (HLS_WRAPPER_RESERVED_SPACE_HIGH - HLS_WRAPPER_RESERVED_SPACE_BASE)

static uint32_t curOffset = HLS_WRAPPER_RESERVED_SPACE_BASE;

static int mem_fd = 0;
static void* reserved_mem;

static int devmem_open = 0;

int open_devmem() {
    mem_fd = open("/dev/mem", O_RDWR | O_SYNC); // will only work if root
    
    reserved_mem = mmap(NULL, HLS_WRAPPER_RESERVED_SPACE_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, mem_fd, (off_t)HLS_WRAPPER_RESERVED_SPACE_BASE);
    if(reserved_mem == MAP_FAILED) {
        //fprintf(stderr, "Unable to mmap reserved memory segment\r\n");
        return 1;
    }
    
    devmem_open = 1;
    
    return 0;
}

void* malloc_fpga(size_t size, uint32_t* offset_ptr) {
    if(!devmem_open)
    {
        if(!open_devmem())
            return NULL;
    }
    if(curOffset + size >= HLS_WRAPPER_RESERVED_SPACE_HIGH)
        return NULL; // alloc fails
    
    *offset_ptr = curOffset;
    void* ret = (void*)((uint32_t)reserved_mem + curOffset - HLS_WRAPPER_RESERVED_SPACE_BASE);
    
    curOffset += (uint32_t)size;
    
    return ret;
    
}

// This function might take long... should be optimized a bit
void* calloc_fpga(size_t nmemb, size_t size, uint32_t* offset_ptr) {
    uint8_t* ret = (uint8_t*)malloc_fpga(nmemb * size, offset_ptr);
    if(!ret)
        return NULL;
    
    for(uint32_t i = 0; i < (uint32_t)nmemb * (uint32_t)size; i++) {
        ret[i] = 0;
    }
    
    return ret;
    
}

void free_fpga() {
    
    munmap(reserved_mem, HLS_WRAPPER_RESERVED_SPACE_SIZE);
}
