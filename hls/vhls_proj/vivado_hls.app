<project xmlns="com.autoesl.autopilot.project" name="vhls_proj" top="hls_wrapper">
    <includePaths/>
    <libraryPaths/>
    <Simulation>
        <SimFlow name="csim" clean="true" csimMode="0" lastCsimMode="0" compiler="true" compilerChoices="clang"/>
    </Simulation>
    <files xmlns="">
        <file name="../../src/tb.cpp" sc="0" tb="1" cflags=" "/>
        <file name="src/in.bmp" sc="0" tb="false" cflags=""/>
        <file name="src/hw_main.cpp" sc="0" tb="false" cflags=""/>
    </files>
    <solutions xmlns="">
        <solution name="master" status="active"/>
    </solutions>
</project>

