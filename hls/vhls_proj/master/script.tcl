############################################################
## This file is generated automatically by Vivado HLS.
## Please DO NOT edit it.
## Copyright (C) 1986-2017 Xilinx, Inc. All Rights Reserved.
############################################################
open_project vhls_proj
set_top hls_wrapper
add_files src/hw_main.cpp
add_files src/in.bmp
add_files -tb src/tb.cpp
open_solution "master"
set_part {xc7z020clg400-1} -tool vivado
create_clock -period 10 -name default
#source "./vhls_proj/master/directives.tcl"
csim_design -clean -compiler clang
csynth_design
cosim_design -compiler gcc
export_design -rtl verilog -format ip_catalog
