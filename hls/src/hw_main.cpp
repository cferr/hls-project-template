/* High-level synthesis functions */

#include <ap_int.h>
#include <string.h>
#include <stdio.h>
#include <stdint.h>

/* Wrapper function : this function starts your accelerator.
 * It is called from the C testbench, during C/RTL cosimulation
 * and when Xhls_wrapper_Start() will be called on the board.
 *
 * For the automated flow to work, it is required that:
 * - there is a single AXI master (but there can be multiple
 *   components using it)
 * - there is a single AXI slave named CTRL_BUS. The various
 *   input arguments will be converted to registers accessible
 *   from outside the accelerator through a memory-mapped AXI
 *   interface.
 *
 * */

// Using preprocessor directives is possible anywhere except
// in pragmas. Here, we define the size of the internal buffer
// in which data is stored locally.
#define BUF_SIZE 240

void hls_wrapper(volatile uint32_t* d_in, volatile uint32_t* d_out,
		volatile uint32_t N, volatile uint32_t resetAfter) {
// AXI Master interfaces' depth must be greater or equal to the
// length of the burst transfers performed. Here, we use BUF_SIZE,
// but we can't use the value of the #define since it's read before
// code is pre-processed.
#pragma HLS INTERFACE m_axi port=d_in offset=slave depth=240
#pragma HLS INTERFACE m_axi port=d_out offset=slave depth=240
#pragma HLS INTERFACE s_axilite port=return bundle=CTRL_BUS
#pragma HLS INTERFACE s_axilite port=N bundle=CTRL_BUS
#pragma HLS INTERFACE s_axilite port=resetAfter bundle=CTRL_BUS

	// This sample code instantiates a memory copy from the host
	// memory through d_in, computes a 3x1 mean, and writes it back
	// through d_out.
	// Since we are processing images, it resets val1, val2, val3 after
	// each end of line.

	uint32_t offset, i, c = 0;
	uint32_t val1 = 0, val2 = 0, val3 = 0;
	uint32_t buf[BUF_SIZE];

	for(offset = 0; offset < N; offset += BUF_SIZE) {
		memcpy(buf, (const uint32_t*)d_in + offset, BUF_SIZE * sizeof(uint32_t));

		for(i = 0; i < BUF_SIZE; ++i, ++c) {
			if(c == resetAfter) {
				c = 0;
				val1 = 0;
				val2 = 0;
				val3 = 0;
			}
			val1 = val2;
			val2 = val3;
			val3 = buf[i];
			// Write after Read dependence will be detected here
			ap_uint<8> b = (((val1 & 0xFF00) >> 8)  + ((val2 & 0xFF00) >> 8) + ((val3 & 0xFF00) >> 8)) / 3;
			ap_uint<8> g = (((val1 & 0xFF0000) >> 16) + ((val2 & 0xFF0000) >> 16) + ((val3 & 0xFF0000) >> 16)) / 3;
			ap_uint<8> r = (((val1 & 0xFF000000) >> 24) + ((val2 & 0xFF000000) >> 24) + ((val3 & 0xFF000000) >> 24)) / 3;
			buf[i] = ((ap_uint<32>)r << 24) | ((ap_uint<32>)g << 16) | ((ap_uint<32>)b << 8);

		}

		memcpy((uint32_t*)d_out + offset, buf, BUF_SIZE * sizeof(uint32_t));
	}

}
