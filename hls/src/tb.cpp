
/* Testbench for accelerator functions */

#include <stdio.h>
#include <stdlib.h>
#include <ap_int.h>

// Prototype for your HLS main function.
void hls_wrapper(volatile uint32_t* d_in, volatile uint32_t* d_out,
		volatile uint32_t N, volatile uint32_t resetAfter);

int main(void)
{
	// Write your test bench here

	// This sample test bench takes a 320x240 image as an input,
	// computes a 3x1 horizontal mean of the pixels.
	// A local and hardware version is provided, both outputs
	// are compared.

	uint32_t* in = (uint32_t*)malloc(320 * 240 * sizeof(uint32_t));
	uint32_t* out = (uint32_t*)calloc(320*240, sizeof(uint32_t));
	uint32_t* out_local = (uint32_t*)calloc(320*240, sizeof(uint32_t));

	FILE* f_in = fopen("in.bmp", "rb");
	FILE* f_out = fopen("out.bmp", "wb");
	FILE* f_out_local = fopen("out.local.bmp", "wb");
	if(!f_in || !f_out) {
		printf("Error on opening input or output file\n");
		return 1;
	}

	uint8_t bmp_header[70];
	size_t hd_read = fread(&bmp_header, sizeof(uint8_t), 70, f_in);
	size_t read = fread(in, sizeof(uint32_t), 320*240, f_in);

	// Call the HLS function
	hls_wrapper(in, out, 320 * 240, 320);

	// Compute a local version
	for(uint16_t i = 0; i < 240; ++i) {
		uint16_t a = 0, r = 0, g = 0, b = 0;
		uint32_t val1 = 0, val2 = 0, val3 = 0;
		for(uint16_t j = 0; j < 320; ++j) {
			val1 = val2;
			val2 = val3;
			val3 = in[320 * i + j];
			b = (((val1 & 0xFF00) >> 8) + ((val2 & 0xFF00)>> 8) + ((val3 & 0xFF00) >> 8)) / 3;
			g = (((val1 & 0xFF0000) >> 16) + ((val2 & 0xFF0000) >> 16) + ((val3 & 0xFF0000) >> 16)) / 3;
			r = (((val1 & 0xFF000000) >> 24) + ((val2 & 0xFF000000) >> 24) + ((val3 & 0xFF000000) >> 24)) / 3;
			out_local[320 * i + j] = (r << 24) | (g << 16) | (b << 8);
		}
	}

	// Compare the result
	uint32_t errors = 0;
	for(uint32_t i = 0; i < 320 * 240; ++i) {
		if(out_local[i] != out[i])
			++errors;
	}

	// Write back the result to a file
	fwrite(&bmp_header, sizeof(uint8_t), 70, f_out);
	fwrite(out, sizeof(uint32_t), 320*240, f_out);

	fwrite(&bmp_header, sizeof(uint8_t), 70, f_out_local);
	fwrite(out_local, sizeof(uint32_t), 320*240, f_out_local);

	// Close the files
	fclose(f_in);
	fclose(f_out);
	fclose(f_out_local);

	printf("There were %d errors\n", errors);

	// Vivado HLS bases its pass/fail test on the return value of
	// the testbench program. Proceed accordingly.
	return (errors > 0);
}
