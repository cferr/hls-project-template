# Generate Vivado HLS project

open_project vhls_proj
open_solution "master"
csynth_design
export_design -rtl vhdl -format ip_catalog -description "HLS Synthesized IP" -vendor "colostate" -library "research" -version "1.0" -taxonomy "/COLOSTATE_IP" -display_name "HLS Project" -ipname "hls"
exit
