# Set the project name
set project_name "hls"
set hdf_name "master_wrapper.hdf"
set bitstream "master_wrapper.bit"
set hwdesc_name "hardware.hdf"

if { [current_project -quiet] != "" } { 
    if { [current_project -quiet] != ${project_name} } {
        close_project
        open_project ${project_name}
    }
} else {
    open_project ${project_name}
}


# create directories if not existing
file mkdir ${project_name}.sdk

# export hardware to sdk
write_hwdef -force ${hwdesc_name}
write_sysdef -force -hwdef ${hwdesc_name} -bitfile ${bitstream} "${project_name}.sdk/${hdf_name}"

exit
