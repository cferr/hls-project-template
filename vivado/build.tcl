# Source of numberOfCPUs : https://stackoverflow.com/questions/29482303/how-to-find-the-number-of-cpus-in-tcl
# Function written by Donal Fellows

proc numberOfCPUs {} { 
    # Windows puts it in an environment variable 
    global tcl_platform env 
    if {$tcl_platform(platform) eq "windows"} { 
        return $env(NUMBER_OF_PROCESSORS) 
    } 

    # Check for sysctl (OSX, BSD) 
    set sysctl [auto_execok "sysctl"] 
    if {[llength $sysctl]} { 
        if {![catch {exec {*}$sysctl -n "hw.ncpu"} cores]} { 
            return $cores 
        } 
    } 

    # Assume Linux, which has /proc/cpuinfo, but be careful 
    if {![catch {open "/proc/cpuinfo"} f]} { 
        set cores [regexp -all -line {^processor\s} [read $f]] 
        close $f 
        if {$cores > 0} { 
            return $cores 
        } 
    }
}


# Set the project name
set project_name "hls"
set hdf_name "master_wrapper.hdf"
set bitstream "master_wrapper.bit"
set hwdesc_name "hardware.hdf"
set try_upgrade_ip 0

if { ![file exist ${project_name}.xpr] } {
    source "create_project.tcl"
} else {
    set try_upgrade_ip 1
}

if { [current_project -quiet] != "" } { 
    if { [current_project -quiet] != ${project_name} } {
        close_project
        open_project ${project_name}
    }
} else {
    open_project ${project_name}
}
# reset project
reset_project

if { ${try_upgrade_ip} == 1 } {
    upgrade_ip [get_ips "*hls*"]
}

# reset runs
#reset_run synth_1
#reset_run impl_1

# build project
launch_runs synth_1 impl_1 -jobs [numberOfCPUs]

# wait for implementation to complete
wait_on_run synth_1
wait_on_run impl_1

# open implemented design
open_run impl_1

# write bitstream
write_bitstream -force -file ${bitstream}

# exit vivado
exit
